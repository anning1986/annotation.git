package com.zan.annotation;

import java.lang.reflect.Method;

public class AnnocationAnalysis {
	
	
	public static void  analysis(){
		//.class反射拿到  Class
		Class<WorldAnnotationTest> clazz = WorldAnnotationTest.class;
		//取clazz中的方法数组
		Method [] method = clazz.getDeclaredMethods();
		for(Method m:method) {
			System.out.println("开始--------------");
			//判断方法上是否有usermapping 注解
			if(m.isAnnotationPresent(UserMapping.class)){
				UserMapping um = m.getAnnotation(UserMapping.class);
				System.out.println("姓名     "+um.name());
				System.out.println("年龄     "+um.age());
			}
			//判断方法上是否有WorldAnnotation 注解
			if(m.isAnnotationPresent(WorldAnnotation.class)){
				WorldAnnotation wa = m.getAnnotation(WorldAnnotation.class);
				System.out.println("国家    "+wa.value());
				System.out.println("省       "+wa.province());
				System.out.println("城市    "+wa.city());
			}
			System.out.println("结束--------------");
		}
		
		
	}
	public static void main (String[] args) {
		analysis();
	}
}
