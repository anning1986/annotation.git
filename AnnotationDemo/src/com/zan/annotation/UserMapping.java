package com.zan.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited  //表明子类可以继承 父类的注解
@Retention(RetentionPolicy.RUNTIME)//编译后可以存在.class 文件中 
@Target(value={ElementType.TYPE,ElementType.METHOD})//指明注解使用的时机
public @interface UserMapping {

	 String name() default "张三";
	 int age() default 12;
}
