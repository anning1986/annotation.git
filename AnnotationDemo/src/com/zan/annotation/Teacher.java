package com.zan.annotation;


@UserMapping
public class Teacher {
	public Teacher() {

	}
	//@UserMapping   UserMapping指定了使用环境，如果释放则报（不允许放在此处exception）
	private String name = "";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@UserMapping(name="李四")
	public void play() {
		System.out.println("Hello world ");
		System.out.println("parents method");
	}
	
	public static void main(String[] args) {
		Teacher t = new Teacher();
		t.play();
	}
}
