package com.zan.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target(value={ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)  
public @interface WorldAnnotation {
     EnumTest value() default EnumTest.China;
     String province() default "JiLin"; 
     String city() default "ChangChun";
	
	
	
	public static enum EnumTest {
		China,American,Eingland
	}
}
