package com.zan.annotation;

import com.zan.annotation.WorldAnnotation.EnumTest;

@WorldAnnotation
public class WorldAnnotationTest {

	
	 public WorldAnnotationTest(){
		 
	 }
	 
	 @WorldAnnotation(value=EnumTest.American)
	 @UserMapping
	 public void world(){
		 System.out.println("this is a world");
	 }
	 
	 @WorldAnnotation(province="北京")
	 @UserMapping(name="李四",age=24)
	 public void province(){
		 System.out.println("hello !this is a province");
	 }
	 
	 @WorldAnnotation(province="山东",city="威海")
	 public void city(){
		 System.out.println("hello ! this is a city");
	 }
}
